package com.kraftwerking.spring.test;

public interface LogWriter {
	public void write(String text);
}
